import "../scss/main.scss";

//import "./modules/scroll-down-btn";
//import "./modules/slider";
//import "./modules/play-video-btn";
//import "./modules/pop-up";
//import "./modules/tabs";
//import "./modules/time-line";
//import "./modules/cta";
import "./modules/indepth-article";
import "./modules/indepth-article/mobileMenu";
import "./modules/indepth-article/chapterMenu";
import "./modules/indepth-article/hero";
