//import Swiper from "swiper";
var Swiper = require('../../../node_modules/swiper/js/swiper');

const paginationContainer = document.getElementById("pagination-container");
const arrPag = [];

const slides = document.getElementsByClassName("swiper-slide");
const arrSlide = Array.prototype.slice.call(slides, 0);

const wrapper = document.getElementById("wrapper");

const slideImg = document.getElementsByClassName("image-container__img");
const arrSlideImg = Array.prototype.slice.call(slideImg, 0);

var swiper = new Swiper(".swiper-container", {
  speed: 1000,

  on: {
    init: function () {
      arrSlide.forEach((elem, index) => {
        const item = document.createElement("span");
        item.className =
          index === 0
            ? "pagination__item pagination__item--active"
            : "pagination__item";
        item.innerHTML = elem.dataset.year;
        item.onclick = () => {
          swiper.slideTo(index, 1000, false);
        };
        if (item.innerHTML !== "undefined") {
          arrPag.push(item);
          paginationContainer.appendChild(item);
        }
      });

      arrSlideImg.forEach((elem, index) => {
        elem.onclick = () => {
          if (index === swiper.realIndex) {
            index++;
          }
          swiper.slideTo(index, 1000, false);
        };
      });
    },
    transitionStart: function () {
      changePag();
      getStubSlide();
    },
    resize: function () {
      swiper.slideTo(0, 1000, false);
    },
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    940: {
      slidesPerView: 6,
    },
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

const changePag = () => {
  arrPag.forEach((elem, index) => {
    arrPag[index].classList.remove("pagination__item--active");
    arrPag[swiper.realIndex].classList.add("pagination__item--active");
    if (index === swiper.realIndex) {
      elem.style.marginRight =
        "calc(50% - " + 67 * (swiper.realIndex + 1) + "px)";
    } else elem.style.marginRight = "0px";
  });
};

const getStubSlide = () => {
  let i = 0;
  console.log(swiper)
  if (
    window.innerWidth >= 940 &&
    swiper.slides[swiper.slides.length - 1].classList.value !==
      "swiper-slide stub-slide"
  ) {
    while (i < 5) {
      swiper.addSlide(
        swiper.slides.length,
        '<div class="swiper-slide stub-slide"></div>'
      );
      i++;
    }
  } else if (
    window.innerWidth < 940 &&
    swiper.slides[swiper.slides.length - 1].classList.value ===
      "swiper-slide stub-slide"
  ) {
    while (i < 5) {
      swiper.removeSlide(swiper.slides.length - 1);
      i++;
    }
  }
};