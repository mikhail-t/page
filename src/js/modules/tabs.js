let arrTabs = [];
const masTabContent = document.getElementsByClassName(
  "we-realize-row__gallary"
);
const arrTabContent = Array.prototype.slice.call(masTabContent, 0);
const tabContainer = document.getElementById("tabContainer");

arrTabContent.forEach((elem, index) => {
  const item = document.createElement("li");
  const btn = document.createElement("button");
  if (index === 0) {
    btn.className = "tabs-container__button tabs-container__button--active";
  } else {
    btn.className = "tabs-container__button";
  }
  item.className = "tabs-container__tabs";
  btn.innerHTML = elem.dataset.tabName;
  btn.onclick = () => {
    changeActiveTab(index);
  };
  item.appendChild(btn);
  tabContainer.appendChild(item);
  arrTabs.push(btn);
});

const changeActiveTab = (index) => {
  arrTabs.forEach((elem) => {
    elem.classList.remove("tabs-container__button--active");
  });
  arrTabContent.forEach((elem) => {
    elem.classList.remove("we-realize-row__gallary--active");
  });
  arrTabContent[index].classList.add("we-realize-row__gallary--active");
  arrTabs[index].classList.add("tabs-container__button--active");
  SwiperTabsBg.slideTo(index + 1, 300, false);
};

var SwiperTabsBg = new Swiper(".swiper-tabs-bg", {
  loop: true,

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },

  on: {
    slideChange: () => {
      if (SwiperTabsBg) {
        changeActiveTab(SwiperTabsBg.realIndex);
      }
    },
  },
});
