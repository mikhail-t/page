/* import * as basicScroll from "basicscroll";
let cta = document.getElementById("cta");
let firstBall = document.getElementById("firstBall");
let secondBall = document.getElementById("secondBall");
const masContainerBall = document.getElementsByClassName("move-container");
const arrContainerBall = Array.prototype.slice.call(masContainerBall, 0);

window.addEventListener("resize", () => {
  resize();
});

const resize = () => {
  ctaInit();

  moveEffectSecond.calculate();
  moveEffectFirst.calculate();
};

const ctaInit = () => {
  if (innerWidth > 1000) {
    firstBall.style.height = cta.offsetHeight * 2 + "px";
    firstBall.style.minWidth = cta.offsetHeight * 2 + "px";

    secondBall.style.height = cta.offsetHeight * 2 + "px";
    secondBall.style.minWidth = cta.offsetHeight * 2 + "px";
  } else {
    firstBall.style.height = cta.offsetHeight + "px";
    firstBall.style.minWidth = cta.offsetHeight + "px";

    secondBall.style.height = cta.offsetHeight + "px";
    secondBall.style.minWidth = cta.offsetHeight + "px";
  }

  arrContainerBall.forEach((elem) => {
    if (innerWidth > 1000) {
      elem.style.width = cta.offsetHeight + "px";
    } else {
      elem.style.width = cta.offsetHeight / 2 + "px";
    }
  });
};

ctaInit();

const moveEffectFirst = basicScroll.create({
  elem: document.querySelector(".moveEffectFirst"),
  from: "top-bottom",
  to: "top-top",
  props: {
    "--firstBall": {
      timing: "quartOut",
      from: "-45%",
      to: "50%",
    },
    "--firstBall2": {
      timing: "quartOut",
      from: "-100%",
      to: "50%",
    },
  },
});

const moveEffectSecond = basicScroll.create({
  elem: document.querySelector(".moveEffectSecond"),
  from: "top-middle",
  to: "top-top",
  props: {
    "--secondBall": {
      timing: "quartOut",
      from: "50%",
      to: "-45%",
    },
  },
});

moveEffectSecond.start();
moveEffectFirst.start();
 */