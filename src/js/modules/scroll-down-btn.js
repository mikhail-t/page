const scrollDownBtn = document.getElementById("scroll-down-btn");
const complitedProject = document.getElementById("complited-project");

scrollDownBtn.onclick = () => {
  complitedProject.scrollIntoView({ behavior: "smooth" });
};
