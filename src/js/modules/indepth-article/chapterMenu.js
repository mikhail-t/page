const chapterMenuWrapper = document.getElementById("chapterMenuWrapper");
const chapterMenu = document.getElementById("chapterMenu");
const openMenuBtn = document.getElementById("menu-btn");
const closeMenuBtn = document.getElementById("closeMenuBtn");

const masChapter = document.getElementsByClassName("chapter");
const arrChapter = Array.prototype.slice.call(masChapter, 0);

const masChapterTitle = document.getElementsByClassName("chapter__title");
const arrChapterTitle = Array.prototype.slice.call(masChapterTitle, 0);

const arrChapterMenuItem = [];
const chapterMenuList = document.getElementById("chapterMenuList");

const scrollBtn = document.getElementById("scroll-down");

window.addEventListener("scroll", (e) => {
  arrChapter.forEach((element, index) => {
    if (document.documentElement.scrollTop > element.offsetTop) {
      arrChapterMenuItem[index].classList.add(
        "chapter-menu-list__item--active"
      );
    } else {
      arrChapterMenuItem[index].classList.remove(
        "chapter-menu-list__item--active"
      );
    }
  });
});

const initChapterMenu = () => {
  arrChapterTitle.forEach((element, index) => {
    const item = document.createElement("li");
    item.className = "chapter-menu-list__item";
    item.onclick = () => {
      element.scrollIntoView({ behavior: "smooth" });
    };

    const itemNumber = document.createElement("p");
    itemNumber.className = "chapter-menu-list-item__number";
    if (index < 9) {
      itemNumber.innerHTML = "0" + (index + 1);
    } else {
      itemNumber.innerHTML = index + 1;
    }

    item.appendChild(itemNumber);

    const itemText = document.createElement("p");
    itemText.className = "chapter-menu-list-item__text";
    itemText.innerHTML = element.innerHTML;
    item.appendChild(itemText);

    arrChapterMenuItem.push(item);
    chapterMenuList.appendChild(item);
  });
};

chapterMenuWrapper.onclick = () => {
  chapterMenuToggle();
};

chapterMenu.onclick = (e) => {
  e.stopPropagation();
};

closeMenuBtn.onclick = () => {
  chapterMenuToggle();
};

openMenuBtn.onclick = () => {
  chapterMenuToggle();
};

const chapterMenuToggle = () => {
  chapterMenuWrapper.classList.toggle("chapter-menu-wrapper--open");
};

scrollBtn.onclick = () => {
  arrChapter[0].scrollIntoView({ behavior: "smooth" });
};

initChapterMenu();
