import 'core-js';
import objectFitPolyfill from "objectFitPolyfill"
var basicScroll = require("../../../../node_modules/basicscroll");
var Rellax = require("rellax");

const heroWrapper = document.getElementById("heroWrapper");
const heroVideo = document.getElementById("video");
const heroBackgroundPoster = document.getElementById("heroBackgroundPoster");
const heroFooter = document.getElementById("hero-footer");
const heroBackground = document.getElementById("heroBackground");
const heroBackgroundMask = document.getElementById("heroBackgroundMask");

window.addEventListener("scroll", function () {
  backgroundAnimation.calculate();
  if (document.documentElement.scrollTop > heroWrapper.offsetHeight || document.body.scrollTop > heroWrapper.offsetHeight) {
    heroVideo.play();
    heroBackgroundPoster.classList.add("hero-bg__poster--start-play")
    heroBackground.classList.add("hero-bg--stiky-position")
    heroFooter.classList.add("hero-footer--stiky-position")
  }
  else if ( document.documentElement.scrollTop <= heroWrapper.offsetHeight ) {
    heroBackground.classList.remove("hero-bg--stiky-position")
    heroFooter.classList.remove("hero-footer--stiky-position")
  }
}, false);

const backgroundAnimation = basicScroll.create({
  elem: document.querySelector(".backgroundAnimation"),
  from: "top-top",
  to: "middle-top",
  inside: (instance, percentage, props) => {
    heroBackgroundMask.style.opacity = 1 - percentage / 100;
  },
});

var rellax = new Rellax(".rellax", {
  speed: -2,
  center: false,
  wrapper: null,
  round: true,
  vertical: true,
  horizontal: false,
});

backgroundAnimation.start();

heroWrapper.classList.add("indepth-article-hero__wrapper--animation");
heroFooter.classList.add("hero-footer--animation");
heroBackground.classList.add("hero-bg--animation");
