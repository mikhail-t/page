const mobileMenuBtn = document.getElementById("mobileMenuBtn");
const mobileMenu = document.getElementById("mobileMenu");
const masDropDown = document.getElementsByClassName("nav-menu__list");
const arrDropDown = Array.prototype.slice.call(masDropDown, 0);

mobileMenuBtn.onclick = () => {
  mobileMenuBtn.classList.toggle("menu-icon-row--open");
  mobileMenu.classList.toggle("mobile-menu");
};

arrDropDown.forEach((element) => {
  element.onclick = () => {
    if (innerWidth < 940) {
      element.classList.toggle("nav-menu__list--open");
    }
  };
});

window.addEventListener("resize", (e) => {
  arrDropDown.forEach((element) => {
    element.classList.remove("nav-menu__list--open");
  });
});
