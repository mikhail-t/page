import Swiper from "swiper";

let arrPaginationItem = document.getElementsByClassName(
  "slide-indicator-row__item"
);
let arrSlide = document.getElementsByClassName("swiper-slide--main");
const indicatorContainer = document.getElementById("slideIndicatior");

var swiperMain = new Swiper(".swiper-main-container", {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next-main",
    prevEl: ".swiper-button-prev-main",
  },
  on: {
    init: () => {
      let arrSlideElem = Array.prototype.slice.call(arrSlide, 0);
      arrSlideElem.pop();
      arrSlideElem.shift();
      arrSlideElem.forEach((elem, index) => {
        const item = document.createElement("span");
        item.className =
          index === 0
            ? "slide-indicator-row__item slide-indicator-row__item--activ"
            : "slide-indicator-row__item";
        item.innerHTML = elem.dataset.slidItem;
        item.dataset.numberSlide = index + 1;
        item.onclick = () => {
          console.log(item.dataset.numberSlide);
          swiperMain.slideTo(item.dataset.numberSlide, 300, false);
        };
        indicatorContainer.appendChild(item);
      });
    },
    slideChange: () => {
      let count;
      let activeIndex;
      if (swiperMain) {
        activeIndex = swiperMain.activeIndex;
        for (count = 0; count < arrPaginationItem.length; count++) {
          if (activeIndex === 0) {
            activeIndex = arrPaginationItem.length;
          } else if (activeIndex - 1 === arrPaginationItem.length) {
            activeIndex = 1;
          }

          if (count === activeIndex - 1) {
            arrPaginationItem[count].className =
              "slide-indicator-row__item slide-indicator-row__item--activ";
          } else {
            arrPaginationItem[count].className = arrPaginationItem[
              count
            ].className.replace("slide-indicator-row__item--activ", "");
          }
        }
      }
    },
  },
});

//secondary slider

var swiper = new Swiper(".swiper-container", {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  navigation: {
    nextEl: ".swiper-button-secondary-next",
    prevEl: ".swiper-button-secondary-prev",
  },
});

{
  let btnModileMenuArea = document.getElementById("menu-btn");
  let btnModileMenu = document.querySelector(".menu-icon");
  let mobileMenu = document.getElementById("menu");
  let body = document.body;

  btnModileMenuArea.onclick = () => {
    btnModileMenu.classList.toggle("menu-icon--open");
    mobileMenu.classList.toggle("menu--open");
    if (mobileMenu.className.indexOf("open") === -1) {
      body.style.overflow = "visible";
    } else {
      body.style.overflow = "hidden";
    }
  };
}
