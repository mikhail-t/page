const callRequestBtn = document.querySelectorAll(".button-request-call");
const callRequestBtnArr = Array.prototype.slice.call(callRequestBtn, 0);
const closePopUpBtn = document.getElementById("closePopUpBtn");
const PopUpLeaveRequest = document.getElementById("popUpLeaveRequest");
const outSidePopUp = document.getElementById("outsidePopUp");

callRequestBtnArr.forEach((elem) => {
  elem.onclick = () => {
    togglePopUP();
  };
});

closePopUpBtn.onclick = () => {
  togglePopUP();
};

outSidePopUp.onclick = () => {
  togglePopUP();
};

PopUpLeaveRequest.onclick = (e) => {
  e.stopPropagation();
};

const togglePopUP = () => {
  outSidePopUp.classList.toggle("outside-pop-up--open");
};
