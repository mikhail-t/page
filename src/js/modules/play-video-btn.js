const playBtn = document.getElementById("play-btn");
const videoFrame = document.getElementById("video-frame");
const onlineControl = document.getElementById("online-control");

playBtn.onclick = () => {
  videoFrame.src = videoFrame.src.concat("&autoplay=1");
  onlineControl.className = onlineControl.className.concat(
    " online-control-row-video--play"
  );
};
